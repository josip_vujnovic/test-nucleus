package com.pyro.testnucleusmvp.presenter;

import android.os.Bundle;

import com.pyro.testnucleusmvp.view.MainFragment;

import java.util.ArrayList;

import nucleus.presenter.RxPresenter;

/**
 * Created by user00 on 14.07.15..
 */

public final class MainPresenter extends RxPresenter<MainFragment> {

    private ArrayList<String> arrayList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        arrayList.add("bok");
        arrayList.add("ti");
        arrayList.add("tamo");

    }

    @Override
    protected void onTakeView(MainFragment view) {
        super.onTakeView(view);
        getView().onItems(arrayList);
    }

    public void request() {

    }
}
