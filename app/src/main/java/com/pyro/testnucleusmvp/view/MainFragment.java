package com.pyro.testnucleusmvp.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.pyro.testnucleusmvp.R;
import com.pyro.testnucleusmvp.presenter.MainPresenter;

import java.util.ArrayList;

import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusFragment;

/**
 * Created by user00 on 14.07.15..
 */

@RequiresPresenter(MainPresenter.class)
public final class MainFragment extends NucleusFragment<MainPresenter> {

    private ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView listView = (ListView) view.findViewById(R.id.list_view);
        adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);
        listView.setAdapter(adapter);
        getPresenter().request();
    }

    public void onItems(ArrayList<String> items){
        adapter.clear();
        adapter.addAll(items);
    }
}
