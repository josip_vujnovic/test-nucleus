package com.pyro.testnucleusmvp.view;

import android.app.Fragment;
import android.os.Bundle;

import com.pyro.testnucleusmvp.FragmentStack;
import com.pyro.testnucleusmvp.R;
import com.pyro.testnucleusmvp.presenter.MainPresenter;

import nucleus.view.NucleusActivity;


public final class MainActivity extends NucleusActivity<MainPresenter> {

    private FragmentStack fragmentStack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentStack = new FragmentStack(getFragmentManager(),R.id.container);

        if (savedInstanceState == null) {
            fragmentStack.replace(new MainFragment());
        }
    }

    public void push(Fragment fragment) {
        fragmentStack.push(fragment);
    }

    @Override
    public void onBackPressed() {
        if (!fragmentStack.pop())
            super.onBackPressed();
    }

    public void replace(Fragment fragment) {
        fragmentStack.replace(fragment);
    }
    
}
